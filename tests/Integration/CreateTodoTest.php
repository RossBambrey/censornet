<?php

namespace App\Tests\Integration;

use App\Tests\TestCase;

class CreateTodoTest extends TestCase
{
    public function testCreateTodoHappyCase()
    {
        $dueDate = new \DateTime('now + 5hours');
        $payload = [
            'title' => 'Todo name',
            'dueDate' => $dueDate->getTimestamp(),
        ];

        $expectedResponse = [
            'title' => 'Todo name',
            'dueDate' => $dueDate->getTimestamp(),
        ];

        $this->post('/todo', $payload)
            ->seeJson($expectedResponse)
            ->seeStatusCode(201);
    }

    public function testCreateInvalidTodo()
    {
        $payload = [];
        $this->post('/todo', $payload)->seeStatusCode(422);
    }
}
