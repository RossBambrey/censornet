<?php

namespace App\Tests\Integration;

use App\Tests\TestCase;

class GetTodoTest extends TestCase
{
    /**
     * @var \DateTime
     */
    private $dueDate;

    /**
     * @var int
     */
    private $todoId;

    protected function setUp()
    {
        parent::setUp();
        $this->dueDate = new \DateTime('now + 2days');
        $payload = [
            'title' => 'Todo name',
            'dueDate' => $this->dueDate->getTimestamp(),
        ];

        $this->post('/todo', $payload);

        $response = json_decode($this->response->getContent());

        $this->todoId = $response->id;
    }

    public function testGetTodo()
    {
        $expectedResponse = [
            'title' => 'Todo name',
            'dueDate' => $this->dueDate->getTimestamp(),
            'dueSoon' => false,
        ];

        $this->get("/todo/{$this->todoId}")
            ->seeJson($expectedResponse)
            ->seeStatusCode(200)
            ->seeJsonStructure(['id','title','dueDate', 'dueSoon', 'overDue', 'complete', 'archived', 'created','updated']);
    }

    public function testGetNonExistentTodo()
    {
        // There will not be an id of 0 so we'll test that
        $this->get("/todo/0")->seeStatusCode(404);
    }

    public function testDueSoonTodo()
    {
        $dueDate = new \DateTime('now + 12hours');
        $payload = [
            'title' => 'Todo name due soon',
            'dueDate' => $dueDate->getTimestamp(),
        ];

        $this->post('/todo', $payload);

        $response = json_decode($this->response->getContent());

        $expectedResponse = [
            'title' => 'Todo name due soon',
            'dueDate' => $dueDate->getTimestamp(),
            'dueSoon' => true,
            'overDue' => false,
        ];

        $this->get("/todo/{$response->id}")
            ->seeJson($expectedResponse)
            ->seeStatusCode(200)
            ->seeJsonStructure(['id','title','dueDate', 'dueSoon', 'overDue', 'complete', 'archived', 'created','updated']);
    }

    public function testOverDueTodo()
    {
        $dueDate = new \DateTime('now - 1hour');
        $payload = [
            'title' => 'Todo name overdue',
            'dueDate' => $dueDate->getTimestamp(),
        ];

        $this->post('/todo', $payload);

        $response = json_decode($this->response->getContent());

        $expectedResponse = [
            'title' => 'Todo name overdue',
            'dueDate' => $dueDate->getTimestamp(),
            'dueSoon' => false,
            'overDue' => true,
        ];

        $this->get("/todo/{$response->id}")
            ->seeJson($expectedResponse)
            ->seeStatusCode(200)
            ->seeJsonStructure(['id','title','dueDate', 'dueSoon', 'overDue', 'complete', 'archived', 'created','updated']);
    }
}
