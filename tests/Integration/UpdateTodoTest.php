<?php

namespace App\Tests\Integration;

use App\Tests\TestCase;

class UpdateTodoTest extends TestCase
{
    public function testUpdateTodo()
    {
        $dueDate = new \DateTime('now + 2days');
        $payload = [
            'title' => 'Todo name',
            'dueDate' => $dueDate->getTimestamp(),
        ];

        $this->post('/todo', $payload);

        $response = json_decode($this->response->getContent());

        $id = $response->id;

        $newDueDate = new \DateTime('now + 6hours');

        $updatePayload = [
            'title' => 'Todo name changed',
            'dueDate' => $newDueDate->getTimestamp(),
        ];

        $expectedResponse = [
            'title' => 'Todo name changed',
            'dueDate' => $newDueDate->getTimestamp(),
        ];

        $this->put("/todo/$id", $updatePayload)
            ->seeJson($expectedResponse);
    }

    public function testCannotUpdateNonExistantTodo()
    {
        $newDueDate = new \DateTime('now + 6hours');

        $updatePayload = [
            'title' => 'Todo name changed',
            'dueDate' => $newDueDate->getTimestamp(),
        ];

        $this->put("/todo/0", $updatePayload)
            ->seeStatusCode(404);
    }

    public function testCannotUpdateWithInvalidData()
    {
        $payload = [];
        $this->put("/todo/1", $payload)
            ->seeStatusCode(422);
    }

    public function testCompleteTodo()
    {
        $dueDate = new \DateTime('now + 5hours');
        $payload = [
            'title' => 'Todo name',
            'dueDate' => $dueDate->getTimestamp(),
        ];

        $this->post('/todo', $payload);

        $response = json_decode($this->response->getContent());

        $id = $response->id;

        $this->put('/todo/complete/' . $id);
        $this->get("/todo/$id")
            ->seeStatusCode(200)
            ->seeJson(['complete' => true]);
    }

    public function testCannotCompleteNonExistentTodo()
    {
        $this->put('/todo/complete/0')
            ->seeStatusCode(404);
    }

    public function testArchiveTodo()
    {
        $dueDate = new \DateTime('now + 5hours');
        $payload = [
            'title' => 'Archive me please',
            'dueDate' => $dueDate->getTimestamp(),
        ];

        $this->post('/todo', $payload);

        $response = json_decode($this->response->getContent());

        $id = $response->id;

        $this->put('/todo/archive/' . $id);
        $this->get("/todo/$id")
            ->seeStatusCode(200)
            ->seeJson(['archived' => true]);
    }
}
