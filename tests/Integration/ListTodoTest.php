<?php

namespace App\Tests\Integration;

use App\Tests\TestCase;

class ListTodoTest extends TestCase
{
    public function testListTodo()
    {
        $expectedResponse['todos'] = [];
        for ($i = 1; $i <= 3; $i++) {
            $dueDate = new \DateTime("now + {$i}days");
            $payload = [
                'title' => "Todo name $i",
                'dueDate' => $dueDate->getTimestamp(),
            ];
            $this->post('/todo', $payload);
        }

        $this->get('/todo')
            ->seeJsonStructure(
                ['todos'=>[
                    ['id','title','dueDate','dueSoon','overDue','complete','archived','created','updated'],
                    ['id','title','dueDate','dueSoon','overDue','complete','archived','created','updated'],
                ]])
            ->seeStatusCode(200);
    }

    public function testListArchived()
    {
        $expectedResponse['todos'] = [];
        for ($i = 1; $i <= 3; $i++) {
            $dueDate = new \DateTime("now + {$i}days");
            $payload = [
                'title' => "Todo name $i",
                'dueDate' => $dueDate->getTimestamp(),
            ];
            $this->post('/todo', $payload);

            $response = json_decode($this->response->getContent());

            $this->put('/todo/archive/'.$response->id);
        }

        $this->get('/todo/archived')
            ->seeJsonStructure(
                ['todos'=>[
                    ['id','title','dueDate','dueSoon','overDue','complete','archived','created','updated'],
                    ['id','title','dueDate','dueSoon','overDue','complete','archived','created','updated'],
                ]])
            ->seeStatusCode(200);
    }

    public function testListCompleted()
    {
        $expectedResponse['todos'] = [];
        for ($i = 1; $i <= 3; $i++) {
            $dueDate = new \DateTime("now + {$i}days");
            $payload = [
                'title' => "Todo name $i",
                'dueDate' => $dueDate->getTimestamp(),
            ];
            $this->post('/todo', $payload);

            $response = json_decode($this->response->getContent());

            $this->put('/todo/complete/'.$response->id);
        }

        $this->get('/todo/completed')
            ->seeJsonStructure(
                ['todos'=>[
                    ['id','title','dueDate','dueSoon','overDue','complete','archived','created','updated'],
                    ['id','title','dueDate','dueSoon','overDue','complete','archived','created','updated'],
                ]])
            ->seeStatusCode(200);
    }
}
