(function ($) {
    // Marking todos as completed
    $('.done').on('click', function (event) {
        event.preventDefault();
        var $parent = $(this).parents('.todo');
        var id = $parent.data('id');

        $.ajax({
            url: '/complete/' + id,
            type: 'POST',
            dataType: 'JSON',
            data: null
        }).done(function () {
            $parent.slideUp("slow", function() {$parent.remove()});
        });
    });

    // Load edit form (done via JS to avoid bootstrap adding irritating blue icons
    // todo - style links properly and dispense with this nonsense
    $('.edit').on('click', function (event) {
        event.preventDefault();
        var $parent = $(this).parents('.todo');
        var id = $parent.data('id');
        window.location.href = '/edit/' + id;
    });

    // Archive a todo
    $('.archive').on('click', function (event) {
        event.preventDefault();
        var $parent = $(this).parents('.todo');
        var id = $parent.data('id');

        $.ajax({
            url: '/archive/' + id,
            type: 'POST',
            dataType: 'JSON',
            data: null
        }).done(function () {
            $parent.slideUp("slow", function() {$parent.remove()});
        });
    });
})(jQuery);
