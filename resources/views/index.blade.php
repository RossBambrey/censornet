@extends('layouts.master')
@section('content')
    <h2>Outstanding tasks</h2>
    <div class="todos">
    @forelse ($todos as $todo)
        <div class="todo col-lg-12 alert-{{ $todo->rowClass }}" data-id="{{ $todo->id }}">
            <div class="col-lg-9" >{{ $todo->title }}</div>
            <div class="col-lg-1">{{ date('d/m/Y', $todo->dueDate) }}</div>
            <div class="col-lg-2">
                <span class="col-lg-4"><i class="fa fa-check-circle-o done" aria-hidden="true" title="Mark as done"></i></span>
                <span class="col-lg-4"><i class="fa fa-pencil-square-o edit" aria-hidden="true" title="Edit"></i></span>
                <span class="col-lg-4"><i class="fa fa-trash-o archive" aria-hidden="true" title="Archive"></i></span>
            </div>
        </div>
    @empty
        <p>Nothing here...</p>
    @endforelse
    </div>
@stop
