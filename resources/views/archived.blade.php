@extends('layouts.master')
@section('content')
    <h2>Archived tasks</h2>
    <div class="todos">
    @forelse ($todos as $todo)
        <div class="todo col-lg-12 alert-info" data-id="{{ $todo->id }}">
            <div class="col-lg-10" >{{ $todo->title }}</div>
            <div class="col-lg-2">{{ date('d/m/Y', $todo->dueDate) }}</div>
        </div>
    @empty
        <p>Nothing here...</p>
    @endforelse
    </div>
@stop
