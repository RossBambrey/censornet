<!doctype html>
<html class="no-js" lang="">
<head>
    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/css/todo.css" rel="stylesheet" type="text/css">

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Ross Bambrey - Censornet portfolio piece</title>
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Ross Bambrey - Censornet portfolio piece</a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="/"><i class="fa fa-fw fa-dashboard"></i> Todo List</a>
                    </li>
                    <li>
                        <a href="/completed"><i class="fa fa-fw fa-check-circle-o"></i> Completed Tasks</a>
                    </li>
                    <li>
                        <a href="/archived"><i class="fa fa-fw fa-trash-o"></i> Archived</a>
                    </li>
                    <li>
                        <a href="/new"><i class="fa fa-fw fa-trash-o"></i> New Task</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class='content'>
                    @yield('content') {{-- This will show the rendered view data --}}
                </div>
            </div>
        </div>
    </div>

<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/todo.js"></script>
</body>
</html>
