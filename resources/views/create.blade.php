@extends('layouts.master')
@section('content')
    <h2>Create a new task</h2>
    {!! Form::open(array('action' => 'HomeController@store')) !!}
    <div class="form-group">
        {!! Form::label('title', 'Task') !!}
        {!! Form::text('title',  null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('dueDate', 'Due') !!}
        {!! Form::date('dueDate',  null, ['class' => 'form-control']) !!}
    </div>
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    {!! Form::close() !!}
@stop
