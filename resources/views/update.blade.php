@extends('layouts.master')
@section('content')
    <h2>Update task {{ $todo->title }}</h2>
    {!! Form::open(array('action' => 'HomeController@store')) !!}
    <div class="form-group">
        {!! Form::label('title', 'Task') !!}
        {!! Form::text('title',  $todo->title, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('dueDate', 'Due') !!}
        {!! Form::date('dueDate',  date('Y-m-d', $todo->dueDate), ['class' => 'form-control']) !!}
    </div>
    {!! Form::hidden('id', $todo->id) !!}
    {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
    {!! Form::close() !!}
@stop
