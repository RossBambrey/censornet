<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Todo
 * @package App\Models
 *
 * Field list
 *
 * id (int)
 * title (string (255))
 * due_date (int) - for storinng unix timestamp
 * complete (bool)
 * archived (bool)
 * updated (datetime)
 * created (datetime)
 */
class Todo extends Model
{
    //
}
