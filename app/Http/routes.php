<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/** Frontend */
Route::get('/', 'HomeController@index');
Route::get('/archived', 'HomeController@archived');
Route::get('/completed', 'HomeController@completed');
Route::get('/new', 'HomeController@create');
Route::get('/edit/{id}', 'HomeController@update');
Route::post('/new', 'HomeController@store');
Route::post('/edit', 'HomeController@store');
Route::post('/complete/{id}', 'HomeController@complete');
Route::post('/archive/{id}', 'HomeController@archive');

/** API */
Route::post('/todo', 'TodoController@create');
Route::get('/todo/archived', 'TodoController@getArchived');
Route::get('/todo/completed', 'TodoController@getCompleted');
Route::get('/todo/{id}', 'TodoController@get');
Route::get('/todo', 'TodoController@index');
Route::put('/todo/{id}', 'TodoController@update');
Route::put('/todo/complete/{id}', 'TodoController@complete');
Route::put('/todo/archive/{id}', 'TodoController@archive');
