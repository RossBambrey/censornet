<?php

namespace App\Http\Controllers\Helpers;

use App\Models\Todo;

/**
 * Class TodoHelper contains static helper classes for todos eg formatting output
 *
 * @todo formatting method requires unit test
 *
 * @package App\Http\Controllers\Helpers
 */
class TodoHelper
{
    const DUE_SOON_THRESHOLD = 86400; // 24 hours

    private function __construct()
    {
        // Should not be instantiated
    }

    /**
     * Format output for dispatching as REST payload
     *
     * @param Todo $todo
     * @return array
     */
    public static function formatOutput(Todo $todo)
    {
        $output = [
            'id' => $todo->id,
            'title' => $todo->title,
            'description' => $todo->description,
            'dueDate' => (int) $todo->due_date,
            'dueSoon' => (bool) self::dueSoon($todo->due_date),
            'overDue' => (bool) self::overDue($todo->due_date),
            'complete' => (bool) $todo->complete,
            'archived' => (bool) $todo->archived,
            'created' => strtotime($todo->created_at),
            'updated' => strtotime($todo->updated_at),
        ];

        return $output;
    }

    /**
     * Is todo due soo (withing DUE_SOON_THRESHOLD)
     *
     * @param int $timestamp
     * @return bool
     */
    private static function dueSoon($timestamp)
    {
        $deltaT = (int) $timestamp - time();
        return ($deltaT < self::DUE_SOON_THRESHOLD) && ($deltaT >= 0);
    }

    /**
     * Has dueDate passed?
     *
     * @param int $timestamp
     * @return bool
     */
    private static function overDue($timestamp)
    {
        $deltaT = (int) $timestamp - time();
        return ($deltaT < 0);
    }
}
