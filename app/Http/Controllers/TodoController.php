<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Helpers\TodoHelper;
use App\Models\Todo;
use App\Services\TodoService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class TodoController is an API controller, all public methods should return JSON responses and be routed by relevant
 * http verbs
 *
 * @package App\Http\Controllers
 */
class TodoController extends Controller
{
    /**
     * @var TodoService
     */
    private $todoService;

    /**
     * TodoController constructor.
     * @param TodoService $todoService
     */
    public function __construct(TodoService $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * Create a todo
     *
     * POST /todo
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $payload = $request->input();
        $todo = $this->todoService->create($payload);

        return new JsonResponse(TodoHelper::formatOutput($todo), 201);
    }

    /**
     * Get single todo
     *
     * GET /todo/{id}
     *
     * @param int $id
     * @return JsonResponse
     */
    public function get($id)
    {
        $todo = $this->todoService->get($id);

        return new JsonResponse(TodoHelper::formatOutput($todo), 200);
    }

    /**
     * List all todos
     *
     * GET /todo
     *
     * @return JsonResponse
     */
    public function index()
    {
        $todos = [];
        /** @var Todo $todo */
        foreach ($this->todoService->getAll() as $todo) {
            $todos[] = TodoHelper::formatOutput($todo);
        }

        $response = [
            'todos' => $todos,
        ];

        return new JsonResponse($response, 200);
    }

    /**
     * Update a todo
     *
     * PUT /todo/{id}
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $payload = $request->input();
        $todo = $this->todoService->update($payload, $id);

        return new JsonResponse(TodoHelper::formatOutput($todo), 200);
    }

    /**
     * Update a todo as completed
     *
     * PUT /todo/complete/{id}
     *
     * @param int $id
     * @return JsonResponse
     */
    public function complete($id)
    {
        $todo = $this->todoService->complete($id);

        return new JsonResponse(TodoHelper::formatOutput($todo), 200);
    }

    /**
     * Update a todo as archived
     *
     * PUT /todo/archive/{id}
     *
     * @param int $id
     * @return JsonResponse
     */
    public function archive($id)
    {
        $todo = $this->todoService->archive($id);

        return new JsonResponse(TodoHelper::formatOutput($todo), 200);
    }

    /**
     * Get all archived todos
     *
     * GET /todo/archived
     *
     * @return JsonResponse
     */
    public function getArchived()
    {
        $todos = [];
        /** @var Todo $todo */
        foreach ($this->todoService->getAllArchived() as $todo) {
            $todos[] = TodoHelper::formatOutput($todo);
        }

        $response = [
            'todos' => $todos,
        ];

        return new JsonResponse($response, 200);
    }

    /**
     * Get all completed todos
     *
     * GET /todo/completed
     *
     * @return JsonResponse
     */
    public function getCompleted()
    {
        $todos = [];
        /** @var Todo $todo */
        foreach ($this->todoService->getAllArchived() as $todo) {
            $todos[] = TodoHelper::formatOutput($todo);
        }

        $response = [
            'todos' => $todos,
        ];

        return new JsonResponse($response, 200);
    }
}
