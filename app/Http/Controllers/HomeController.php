<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;
use App\Services\CallableTrait;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class HomeController frontend controller
 *
 * This class serves as an example as to how the Todo API could be consumed
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    use CallableTrait;

    /**
     * Get all non archived and incomplete todos
     */
    public function index()
    {
        $todos = $this->call('/todo', 'GET');
        return view('index')->with('todos', $this->addClasses($todos->todos));
    }

    /**
     * Get all archived todos
     */
    public function archived()
    {
        $todos = $this->call('/todo/archived', 'GET');
        return view('archived')->with('todos', $todos->todos);
    }

    /**
     * Get all completed todos
     */
    public function completed()
    {
        $todos = $this->call('/todo/completed', 'GET');
        return view('completed')->with('todos', $todos->todos);
    }

    /**
     * Complete a todo
     *
     * Designed to be used as an ajax call, just returns a success message
     *
     * @todo Return something useful if an error is thrown
     *
     * @param int $id
     * @return JsonResponse
     */
    public function complete($id)
    {
        $this->call("/todo/complete/$id", 'PUT');
        return new JsonResponse(['message' => 'Success']);
    }

    /**
     * Archive a todo
     *
     * Designed to be used as an ajax call, just returns a success message
     *
     * @todo Return something useful if an error is thrown
     *
     * @param int $id
     * @return JsonResponse
     */
    public function archive($id)
    {
        $this->call("/todo/archive/$id", 'PUT');
        return new JsonResponse(['message' => 'Success']);
    }

    /**
     * Get the create a new todo form
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Get the todo update form
     *
     * @param int $id
     */
    public function update($id)
    {
        $todo = $this->call('/todo/'.$id, 'GET');
        return view('update')->with('todo', $todo);
    }

    /**
     * Store (create or update) a todo
     *
     * @todo Do something useful with validation errors
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $payload = $request->input();
        $payload['dueDate'] = strtotime($payload['dueDate']);
        if ($request->input('id')) {
            $this->call('/todo/'.$request->input('id'), 'PUT', $payload);
        } else {
            $this->call('/todo', 'POST', $payload);
        }

        return new RedirectResponse('/');
    }

    /**
     * Get some bootstrap classes depending on dueDate
     *
     * @todo This really needs to be moved to a service of its own, preferable with an interface - we could want to
     *       Swap out themes/frameworks and would need to provide different implementations
     *
     * @param Todo[] $todos
     * @return Todo[]
     */
    private function addClasses($todos)
    {
        foreach ($todos as $index => $todo) {
            if ($todo->complete) {
                $todo->rowClass = "info"; // Blue - for completed and archived
            } elseif ($todo->overDue) {
                $todo->rowClass = "danger"; // Red for overdue
            } elseif ($todo->dueSoon) {
                $todo->rowClass = "warning"; // Orange - due in next 24 hours
            } else {
                $todo->rowClass = "success"; // Green - it's fine, plenty of time
            }
            // Update the array
            $todos[$index] = $todo;
        }

        return $todos;
    }
}
