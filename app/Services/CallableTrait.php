<?php

namespace App\Services;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

/**
 * Class CallableTrait Provide calling functions for controllers to consume an internal API
 * @package App\Services
 */
trait CallableTrait
{
    /**
     * Call an internal uri as a RESTful API
     *
     * @param string $uri
     * @param string $method
     * @param array $parameters
     * @return object
     */
    private function call($uri, $method, $parameters = [])
    {
        $request = Request::create($uri, $method, $parameters);
        $request->headers->set('Content-Type', 'application/json');
        return json_decode(Route::dispatch($request)->getContent());
    }
}
