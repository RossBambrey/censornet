<?php

namespace App\Services;

use App\Models\Todo;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class TodoService handle business logic of todos
 * @package App\Services
 */
class TodoService
{
    /**
     * @var ValidationService
     */
    private $validationService;

    /**
     * TodoService constructor.
     * @param ValidationService $validationService
     */
    public function __construct(ValidationService $validationService)
    {
        $this->validationService = $validationService;
    }

    /**
     * Create a new todo entry
     *
     * @param array $payload
     * @return Todo
     */
    public function create(array $payload)
    {
        $this->validationService->validate($payload, $this->getValidationRules());

        $todo = new Todo();
        $todo->title = $payload['title'];
        $todo->due_date = strtotime($payload['dueDate']);

        $todo->save();

        return $todo;
    }

    /**
     * Get an existing todo from its id
     *
     * @param int $id
     * @return Todo
     * @throws ModelNotFoundException
     */
    public function get($id)
    {
        $todo = Todo::findOrFail($id);

        return $todo;
    }

    /**
     * Get all non-archived, open (i.e. not completed) todos
     *
     * @return Todo[]
     */
    public function getAll()
    {
        $todos = Todo::where('complete', false)
            ->where('archived', false)
            ->orderBy('due_date', 'ASC')->get();

        return $todos;
    }

    /**
     * Get all archived todos
     *
     * @return Todo[]
     */
    public function getAllArchived()
    {
        $todos = Todo::where('archived', 1)
            ->orderBy('due_date', 'ASC')->get();

        return $todos;
    }

    /**
     * Get all completed todos
     * @return Todo[]
     */
    public function getAllCompleted()
    {
        $todos = Todo::where('complete', 1)
            ->orderBy('due_date', 'ASC')->get();

        return $todos;
    }

    /**
     * Update a todo
     *
     * @param array $payload
     * @param int $id
     * @return Todo
     * @throws ModelNotFoundException
     */
    public function update(array $payload, $id)
    {
        $this->validationService->validate($payload, $this->getValidationRules());

        $todo = Todo::findOrFail($id);
        $todo->title = $payload['title'];
        $todo->due_date = $payload['dueDate'];

        $todo->save();

        return $todo;
    }

    /**
     * Mark a todo as completed
     *
     * @param int $id
     * @return Todo
     */
    public function complete($id)
    {
        $todo = Todo::findOrFail($id);
        $todo->complete = 1;
        $todo->save();

        return $todo;
    }

    /**
     * Mark a todo as archived
     *
     * @param int $id
     * @return Todo
     */
    public function archive($id)
    {
        $todo = Todo::findOrFail($id);
        $todo->archived = 1;
        $todo->save();

        return $todo;
    }

    /**
     * Get validation rules for Todo
     *
     * @todo Move this to model as private static?
     *
     * @return array
     */
    private function getValidationRules()
    {
        return [
            'title' => 'required',
            'dueDate' => 'required',
        ];
    }
}
