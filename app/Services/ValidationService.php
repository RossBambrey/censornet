<?php

namespace App\Services;

use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

/**
 * Class ValidationService
 *
 * @todo this needs some unit tests writing
 *
 * @package App\Services
 */
class ValidationService
{
    /**
     * @var Factory
     */
    private $validator;

    /**
     * ValidationService constructor.
     * @param Factory $validator
     */
    public function __construct(Factory $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Run validation
     *
     * @param array $data
     * @param array $rules
     * @throws ValidationException
     */
    public function validate(array $data, array $rules)
    {
        $validator = $this->validator->make($data, $rules);
        if ($validator->fails()) {
            throw new ValidationException($validator->getMessageBag());
        }
    }
}
