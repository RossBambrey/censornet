Censornet portfolio piece
=========================

_By Ross R. Bambrey_

Overview
--------

This proof of concept app is separated between an API layer (based around TodoController) and a web layer (based around 
HomeController). Requests to the API are done internally via the `App\Services\CallableTrait`.  

There are many possible improvements (and indeed test cases for the API) that can be made to this proof of concept, I 
have compiled a (non-exhaustive) list at the end of this document

Installation
------------

####Application

Run

    composer install
    
This, strictly speaking, will be unnecessary given that the code is delivered tarred and zipped so all vendor directory 
entries and autoloaders will be distributed with the code. 
    
####Virtual hosts

Set up basic vhost, something like *apache*

```apache
<VirtualHost *:80>
	ServerName csnet.vcap.me
	DocumentRoot "/vagrant/app/public"
  	<Directory "/vagrant/app/public">
       AllowOverride all
       Require all granted
  	</Directory>
</VirtualHost>
```

or *nginx*

```nginx
server_name csnet.vcap.me;
root /vagrant/app/public;
```

These rely on the vcap.me DNS entry, add this to local /etc/hosts file if working offline.

####Database

The database only has one table. For simplicity the db user and password for for dev is set to root:root and the db name
is censornet. To set this up create a new database:

    echo "CREATE SCHEMA censornet" | mysql -uroot -p
    
and enter the password when created. If you want to use different db credentials then alter `app/.env`. 

Once the new db is created, run the migrations

    php artisan migrate
    
####Running tests

There are some tests setup up for end to end testing of the API layer. To run these from app directory run

    php vendor/bin/phpunit
    
This should give

    OK (15 tests, 128 assertions)
    
If you don't refresh the migrations at this point the db will be prepopulated with rubbish test data. To start with an 
empty db run

    php artisan migrate:refresh
    
####The environment

This was developed using a vagrant/virtualbox vm with 2 cores and 2gigs ram installed with trusty64 (ubuntu 14.04 LTS) 
which was updated and upgraded on 01/08/2016. To recreate the dev box run

```bash
apt-get update
apt-get upgrade --fix-missing
apt-get install -y php5 php5-mysql mysql-server apache2 htop vim libapache2-mod-php5
```

Also install composer by following instructions at https://getcomposer.org/

Future development / known shortfallings
----------------------------------------

- API requires authentication
- CSRF middleware was removed for simplicity
- API Test coverage is not complete
- I'd prefer to use Doctrine rather than Eloquent as an ORM. This would make service layer testing easier if we 
setup interfaces for the repositories and allowing easy use of mocks via the contructor injection
- Moving the API tests to test only the service layer with vastly improve performance of tests, especially when coupled
with mocking the db layer
- The frontend would probably be nicer written in something like AngularJS or Ember. It was written in Laravel here
purely as an example of how the API could be consumed.
- Need to set up ansible playbook for provisioning development environment
- Would be better to set up some ant tasks for running test suites where a typical build task could:
    - clear down database
    - run migrations
    - run standard reporting tools like linting, mess detector, code sniffer
    - run test suite
    - reset db to empty state
    - Could also add a dev enviroment db seeder with some useful data for dev then add an ant task to run `php artisan db:seed`
- The above tasks can then be adapted for use in a CI environment like Jenkins
- As a last note there is definitely some more refactor to be done, particularly in the test suites. Particularly
I think setting up an `ActionInterface` that could be implemented by concrete classes that execute actions that
set up known data (e.g. create a todo with a known dueDate). Generally the current tests have far too much copy and paste.
- A final final gripe is that todo is a terrible name for the entity as phpdoc recognises todo in docblocks as a 
todo item. I think renaming them tasks is probably good for the sanity.
